<meta http-equiv=?Content-Type? content=?text/html; charset=UTF-8? />
<head>
<title>Sistema de votaci&oacute;n</title>

<style>
* {
  box-sizing: border-box;
}
body, input, select {
  font-family:Helvetica,Arial,sans-serif;
  font-size: 14px;
}
select, input { margin: 0; border: 1px solid gray; }
.error {color: #FF0000;}
</style>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
</head>

<body>

<h1>FORMULARIO DE VOTACI&Oacute;N</h1>

    <?php
    $nameErr = $emailErr = $aliasErr = $rutErr = $seleccionesErr = $regionErr = $comunaErr = $candidatoErr = "";
    $name = $alias = $rut = "";
    $selecciones = 0;
    $comuna_actual = 0;
    $comuna = $email = $region= $candidato = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $nameErr = "Se requiere nombre o apellido";
        } else {
            $name = test_input($_POST["name"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z-' ]*$/", $name)) {
                $nameErr = "Only letters and white space allowed";
            }
        }
        // requiere letras y números, length(alias)>5
        if (empty($_POST["alias"])) {
            $aliasErr = "Se requiere un Alias";
        } else {
            $alias = test_input($_POST["alias"]);

            if (strlen($alias) <6) {
                $aliasErr = "Alias debe tener mas de 5 caracteres";
            } else {
                if (preg_match('/[A-Za-z]/', $alias) && preg_match('/[0-9]/', $alias)) {
                    $aliasErr = "";
                } else {
                    $aliasErr = "El alias debe contener al menos una letra y un numero";
                }
            }
        }
        if (empty($_POST["rut"])) {
            $rutErr = "Se requiere ingresar RUT";
        } else {
            $rut = test_input($_POST["rut"]);
            if (!valida_rut($rut)) {
                $rutErr = "Formato de rut invalido";
            }
        }
        if (empty($_POST["email"])) {
            $emailErr = "Se requiere Email";
        } else {
            $email = test_input($_POST["email"]);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Formato de email incorrecto";
            }
        }

        $web = isset($_POST['web']) ? $_POST['web'] : false;
        $tv = isset($_POST['tv']) ? $_POST['tv'] : false;
        $rrss = isset($_POST['rrss']) ? $_POST['rrss'] : false;
        $amigo = isset($_POST['amigo']) ? $_POST['amigo'] : false;

        if ($web) $selecciones = 1;
        if ($tv) $selecciones = $selecciones + 1;
        if ($rrss) $selecciones = $selecciones + 1;
        if ($amigo) $selecciones = $selecciones + 1;

        if ($selecciones <2) {
            $seleccionesErr="Se deben seleccionar al menos 2 opciones";
        }
        $region = test_input($_POST["region"]);
        if (empty($_POST["region"])) {
            $regionErr = "Se requiere ingresar regi&oacute;n";
        } else {
            $region = $_POST["region"];
            $regionErr = "";
        }
        $candidato = test_input($_POST["candidato"]);
        if (empty($_POST["candidato"])) {
            $candidatoErr = "Se requiere ingresar candidato";
        } else {
            $candidato = $_POST["candidato"];
            $candidatoErr = "";
        }
        if (empty($_POST["comuna"])) {
            $comunaErr = "Se requiere ingresar comuna";
        } else {
            $comuna = $_POST["comuna"];
            $comunaErr = "";
        }

    }
    ?>

    <!-- form method="post" action="./VotacionAction.php" -->

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <table>
        <tr><td><label for="name">Nombre y Apellido:</label></td><td><input type="text" id="name" name="name" value="<?php echo $name;?>" style="width:100%"></td>
            <td width="340px"><span class="error">* <?php echo $nameErr;?></span></td></tr>

        <tr><td><label for="alias">Alias</label></td><td><input type="text" id="alias" name="alias" value="<?php echo $alias;?>" style="width:100%"></td>
            <td width="400px"><span class="error">* <?php echo $aliasErr;?></span></td></tr>

        <tr><td><label for="rut">RUT</label></td><td><input type="text" id="rut" name="rut" value="<?php echo $rut;?>" style="width:100%">  </td>
            <td width="400px"><span class="error">* <?php echo $rutErr;?></span></td></tr>

        <tr><td><label for="email">Email</label></td><td><input type="text" id="email" name="email" value="<?php echo $email;?>" style="width:100%">  </td>
            <td width="400px"><span class="error">* <?php echo $emailErr;?></span></td></tr>

    <?php
    $servername = "localhost";
    $username   = "root";
    $password   = "admin";
    $dbname     = "votaciones";

    // Crea conexion
    $conn = new mysqli($servername, $username, $password, $dbname);

    // valida conexión
    if ($conn->connect_error) {
        die("Error en la conexion: " . $conn->connect_error);
    }

    // Consulta todos las regiones existentes en la base de datos
    $sql = "SELECT * FROM region";
    $result = $conn->query($sql);
    ?>

	<tr><td><label for="region">Regi&oacute;n</label></td>
    <td>
	<select id="regionID" name="region" style="width:100%">
    <option value="">Seleccione una regi&oacute;n</option>
    <?php
    $id = "";
    $nombre = "";
    if ($result->num_rows > 0) {
        // Muestra las regiones en un combo box
        while ($row = $result->fetch_assoc()) {
            $id = $row["id_region"];
            $nombre = $row["nombre"];
            echo '<option value="';
            echo $id;
            echo '"';
            if ($id == $region) {
                echo " selected='selected'";
            }
            echo '">';
            echo $nombre;
            echo '</option>';
        }
    }
    ?>
    </select></td><td width="400px"><span class="error">* <?php echo $regionErr;?></span></td></tr>

    <!-- Reserva espacio para mostrar las comunas en un combobox -->
    <tr><td><label>Comuna:</label><td><label class="form-select" id="show" name="comuna">Comuna</label></td>
    </td><td width="400px"><span class="error">* <?php echo $comunaErr;?></span></td></tr>

    <?php
    // Consulta los candidatos existente en la base de datos
    $sql = "SELECT * FROM candidato";
    $result = $conn->query($sql);
    ?>

    <tr><td><label for="candidato">Candidato</label></td>
        <td>
            <select id="candidato" name="candidato" style="width:100%">
                <option value=''>Seleccione un candidato</option>
    <?php
    $id_candidato = "";
    $nombre = "";

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $id_candidato = $row["id_candidato"];
            $nombre = $row["nombre"];
            echo "<option value='";
            echo $id_candidato;
            echo "'";
            if ($candidato == $id_candidato) {
                echo " selected='selected'";
            }
            echo ">";
            echo $nombre;
            echo "</option>";
        }
    }
    function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    function valida_rut($r)
    {
        $r=preg_replace("/[^0-9]/", '', $r);
        $sub_rut=substr($r,0,strlen($r)-1);
        $sub_dv=substr($r,-1);
        $x=2;
        $s=0;
        for ( $i=strlen($sub_rut)-1;$i>=0;$i-- )
        {
            if ( $x >7 )
            {
                $x=2;
            }
            $s += $sub_rut[$i]*$x;
            $x++;
        }
        $dv=11-($s%11);
        if ( $dv==10 )
        {
            $dv='K';
        }
        if ( $dv==11 )
        {
            $dv='0';
        }
        if ( $dv==$sub_dv )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    ?>
    </select></td><td width="400px"><span class="error">* <?php echo $candidatoErr;?></span></td></tr>

    <tr><td><label for="last">C&oacute;mo se enter&oacute; de Nosotros </label></td>

    <td><label class="container">Web
      <input type="checkbox" name="web" <?php if(isset($web) && $web == 'on') echo 'checked'; ?> >
      <span class="checkmark"></span>
    </label>
    <label class="container">TV
      <input type="checkbox" name="tv" <?php if(isset($tv) && $tv == 'on') echo 'checked'; ?>>
      <span class="checkmark"></span>
    </label>
    <label class="container">Redes Sociales
      <input type="checkbox" name="rrss" <?php if(isset($rrss) && $rrss == 'on') echo 'checked'; ?> >
      <span class="checkmark"></span>
    </label>
    <label class="container">Amigo
      <input type="checkbox" name="amigo" <?php if(isset($amigo) && $amigo == 'on') echo 'checked'; ?> >
      <span class="checkmark"></span>
    </label></td><td width="400px"><span class="error">* <?php echo $seleccionesErr;?></span></td></tr>
    </table>
    <br><br>
    <input name="submit" id="submit" type="submit" value="Votar"></input>
</form>

    <?php
        if(isset($_POST['submit'])) {
            // valida si hay errores

            if ($nameErr == "" && $emailErr == "" && $aliasErr == "" && $rutErr == "" &&  $seleccionesErr == "" && $regionErr == "" && $candidatoErr == "" && $comunaErr == "") {

                // Se valida que no existan votos para el rut seleccionados
                // Crea conexion
                $conn = new mysqli($servername, $username, $password, $dbname);
                // valida conexión
                if ($conn->connect_error) {
                    die("Error en la conexion: " . $conn->connect_error);
                }
                // Consulta todos los items para ese rut
                $sql = "SELECT id, nombre, alias, rut, email, region, comuna, candidato, web, tv, rrss, amigo FROM votos";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {
                        $rut_bd = $row["rut"];
                        if($rut==$rut_bd) {
                            // ya existe en la base de datos
                            echo '<script>alert("Ya existe este rut en la base de datos")</script>';
                            exit();
                        }
                    }
                }

                // Ahora se puede ingresar el voto con  los datos del formulario
                // Calcula el último "id" de la tabla votos
                $sql = "SELECT * FROM votos";
                $last_id = 0;
                // Crea conexion
                $conn = new mysqli($servername, $username, $password, $dbname);
                // valida conexión
                if ($conn->connect_error) {
                    die("Error en la conexion: " . $conn->connect_error);
                }
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    // output data of each row
                    while ($row = $result->fetch_assoc()) {
                        $last_id++;
                    }

                }
                else {
                    alert("0 results");
                }
                $last_id++;

                // Cambio rápido para guardar correctamente los booleanos en base de datos
                if ($tv == "on") {
                    $tv = "1";
                } else {
                    $tv = "0";
                }

                if ($web== "on") {
                    $web = "1";
                } else {
                    $web = "0";
                }

                if ($rrss == "on") {
                    $rrss = "1";
                } else {
                    $rrss = "0";
                }

                if ($amigo == "on") {
                    $amigo = "1";
                } else {
                    $amigo = "0";
                }

                // Inserta para el item id+1
                $sql = "INSERT INTO `votos`(`id`, `nombre`, `alias`, `rut`, `email`, `region`, `comuna`, `candidato`, `web`, `tv`, `rrss`, `amigo`) VALUES ('$last_id', '$name', '$alias', '$rut', '$email', '$region', '$comuna', '$candidato', '$web', '$tv', '$rrss', '$amigo')";
                $result = $conn->query($sql);

                if ($result === TRUE) {
                    // echo "Registros agregados correctamente";
                    echo "<h3 color = #FF0001> <b>Se ha realizado el voto</b> </h3>";
                    echo "<h3>Datos:</h3>";
                    echo "Nombre: " .$name;
                    echo "<br>";
                    echo "Alias: " .$alias;
                    echo "<br>";
                    echo "RUT: " .$rut;
                    echo "<br>";
                    echo "Email: " .$email;
                    echo "<br>";
                    echo "Region: " .$region;
                    echo "<br>";
                    echo "Comuna: " .$comuna;
                    echo "<br>";
                    echo "Candidato: " .$candidato;
                    echo "<br>";
                    echo "Se enter&oacute; por: ";
                    if ($web == "1") { echo "Web / "; };
                    if ($tv == "1") { echo " TV / "; };
                    if ($rrss == "1") { echo " RR.SS. / "; };
                    if ($amigo == "1") { echo " Por amigos"; };

                    echo "<br>";
                    echo "<h3>Ingreso exitoso</h3>";

                    $region = "";
                    $candidato = "";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }

                $conn->close();





            }
            else {
                echo "Hay errores por resolver";
            }
        };
    ?>
<script>
    $(document).ready(function() {
        $('#regionID').change(function(){
            var myid = $('#regionID').val();
debugger;
            $.ajax({
                type: 'POST',
                url: 'getcomunas.php',
                data: {id: myid},
                success: function (data)
                {
                    $('#show').html(data);
                }
            });
        });
    });
</script>
</body>
