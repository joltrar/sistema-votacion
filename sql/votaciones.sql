-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-01-2024 a las 00:39:21
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `votaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidato`
--

CREATE TABLE `candidato` (
  `id_candidato` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `candidato`
--

INSERT INTO `candidato` (`id_candidato`, `nombre`) VALUES
(1, 'Albert Einstein'),
(2, 'Paul Dirac'),
(3, 'Nikola Tesla'),
(4, 'Salvador Dalí'),
(5, 'Joan Miró'),
(6, 'Pablo Picasso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comuna`
--

CREATE TABLE `comuna` (
  `id_comuna` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `id_region` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comuna`
--

INSERT INTO `comuna` (`id_comuna`, `nombre`, `id_region`) VALUES
(1, 'Alto Hospicio', 1),
(2, 'Iquique', 1),
(3, 'Camiña', 1),
(4, 'Colchane', 1),
(5, 'Huara', 1),
(6, 'Pica', 1),
(7, 'Pozo Almonte', 1),
(8, 'Antofagasta', 2),
(9, 'Mejillones', 2),
(10, 'Sierra Gorda', 2),
(11, 'Taltal', 2),
(12, 'Calama', 2),
(13, 'Ollague', 2),
(14, 'San Pedro de Atacama', 2),
(15, 'María Elena', 2),
(16, 'Tocopilla', 2),
(17, 'Chañaral', 3),
(18, 'Diego de Almagro', 3),
(19, 'Caldera', 3),
(20, 'Copiapó', 3),
(21, 'Tierra Amarilla', 3),
(22, 'Alto del Carmen', 3),
(23, 'Freirina', 3),
(24, 'Huasco', 3),
(25, 'Vallenar', 3),
(26, 'Canela', 4),
(27, 'Illapel', 4),
(28, 'Los Vilos', 4),
(29, 'Salamanca', 4),
(30, 'Andacollo', 4),
(31, 'Coquimbo', 4),
(32, 'La Higuera', 4),
(33, 'La Serena', 4),
(34, 'Paihuano', 4),
(35, 'Vicuña', 4),
(36, 'Combarbalá', 4),
(37, 'Monte Patria', 4),
(38, 'Ovalle', 4),
(39, 'Punitaqui', 4),
(40, 'Río Hurtado', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `region`
--

CREATE TABLE `region` (
  `id_region` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `region`
--

INSERT INTO `region` (`id_region`, `nombre`) VALUES
(1, 'Tarapacá'),
(2, 'Antofagasta'),
(3, 'Atacama'),
(4, 'Coquimbo'),
(5, 'Valparaíso'),
(6, 'Libertador General Bernardo O\'Higgins'),
(7, 'Maule'),
(8, 'Bío Bío'),
(9, 'Araucanía'),
(10, 'Los Lagos'),
(11, 'Aysén del General Carlos Ibañez del Campo'),
(12, 'Magallanes y la Antártica chilena'),
(13, 'Metropolitana de Santiago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votos`
--

CREATE TABLE `votos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `rut` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `region` int(11) DEFAULT NULL,
  `comuna` int(11) DEFAULT NULL,
  `candidato` int(11) DEFAULT NULL,
  `web` tinyint(4) DEFAULT NULL,
  `tv` tinyint(4) DEFAULT NULL,
  `rrss` tinyint(4) DEFAULT NULL,
  `amigo` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `votos`
--

INSERT INTO `votos` (`id`, `nombre`, `alias`, `rut`, `email`, `region`, `comuna`, `candidato`, `web`, `tv`, `rrss`, `amigo`) VALUES
(1, 'Pedro Perez', 'pietro2024', '11111111-1', 'pperez@mail.com', 1, 2, 2, 1, 1, 0, 0),
(2, 'Mariana Alvarez', 'maryanne15', '15955783-4', 'mary@mail.com', 3, 20, 1, 1, 0, 1, 1),
(3, 'Julio Gomez', 'segundo2024', '12345678-5', 'jgomez@mail.com', 2, 15, 2, 1, 1, 1, 0),
(4, 'Jorge Silva', 'jsiva2000', '8583741-9', 'jsilvao@mail.com', 3, 18, 3, 1, 1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`id_candidato`);

--
-- Indices de la tabla `comuna`
--
ALTER TABLE `comuna`
  ADD PRIMARY KEY (`id_comuna`);

--
-- Indices de la tabla `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id_region`);

--
-- Indices de la tabla `votos`
--
ALTER TABLE `votos`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
